'use strict';

export default {
	compileEnhancements: false,
	extensions: ['ts'],
	require: ['ts-node/register'],
	sources: ['**/*.ts'],
	files: ['test/**/*.ts'],
};
