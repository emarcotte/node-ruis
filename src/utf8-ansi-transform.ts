import {Transform, TransformCallback} from 'stream';

export type ANSIEvent = {kind: 'text'; text: string}
| {kind: 'key'; key: number}
| {kind: 'CSI'; CSI: string}
| {kind: 'alt'; key: string}
| {kind: 'EOF'; EOF: true};

/**
 * Map some common names to byte sequences we'll get. Note that some control
 * sequences overlap with regular keys these days (ctrl-m = enter, ctrl-i = tab,
 * etc).
 *
 * @see https://www.ibm.com/support/knowledgecenter/SSAT4T_15.1.3/com.ibm.xlf1513.lelinux.doc/language_ref/asciit.html
 */
export const Keys: { [index: string]: number } = {
	KEY_NULL: 0x00,
	CTRL_A: 0x01,
	CTRL_B: 0x02,
	CTRL_C: 0x03,
	CTRL_D: 0x04,
	CTRL_E: 0x05,
	CTRL_F: 0x06,
	CTRL_H: 0x08,
	TAB: 0x09,
	NEWLINE: 0x0A,
	CTRL_K: 0x0B,
	CTRL_L: 0x0C,
	ENTER: 0x0D,
	CTRL_N: 0x0E,
	CTRL_P: 0x10,
	CTRL_T: 0x14,
	CTRL_U: 0x15,
	CTRL_W: 0x17,
	BACKSPACE: 0x7F,
};

export const ESC = 0x1B;

/** Keys by key code instead of name. */
export const KeyCode: {[index: number]: string} = Object.assign(
	{}, ...Object.entries(Keys).map(([k, v]) => ({[v]: k}))
);

/** Parse ANSI and UTF8 sequences. Return discrete events. */
export class UTF8ANSITransform extends Transform {
	private _internalBuffer = Buffer.alloc(0);

	public constructor() {
		super({
			writableObjectMode: false,
			readableObjectMode: true,
			allowHalfOpen: false,
		});
	}

	/**
	 * Determines if we have anything left to transform.
	 */
	public hasOutstandingInput(): boolean {
		return this._internalBuffer.length != 0;
	}

	public _transform(data: Buffer, _: string, done: TransformCallback): void {
		this._internalBuffer = Buffer.concat([this._internalBuffer, data]);
		for (let event = this._processByte(); event; event = this._processByte()) {
			this.push(event);
		}

		done();
	}

	/**
	 * If we're flushing we've already gotten all the bytes and have nothing
	 * let to offer, just call it a day.
	 * @param done Finish callback.
	 */
	public _flush(done: TransformCallback): void {
		this.push({kind: 'EOF', EOF: true});
		done();
	}

	/**
	 * Try to generate a complete event sequence from the current buffer.
	 * @return The final sequence or null.
	 */
	private _processByte(): ANSIEvent | null {
		const firstByte = this._internalBuffer[0];
		const matchedKey = KeyCode[firstByte];
		if (!firstByte) {
			return null;
		}

		if (matchedKey) {
			this._internalBuffer = this._internalBuffer.slice(1);
			return {kind: 'key', key: firstByte};
		}

		if (firstByte === ESC) {
			return this._parseEscapeSequence();
		}

		// https://unicodebook.readthedocs.io/guess_encoding.html#is-utf8
		if (firstByte >= 0xC2 && firstByte <= 0xDF) {
			/* 0b110xxxxx: 2 bytes sequence */
			return this._textSlice(2);
		}

		if (firstByte >= 0xE0 && firstByte <= 0xEF) {
			/* 0b1110xxxx: 3 bytes sequence */
			return this._textSlice(3);
		}

		if (firstByte >= 0xF0 && firstByte <= 0xF4) {
			/* 0b11110xxx: 4 bytes sequence */
			return this._textSlice(4);
		}

		// Return the character if its valid or invalid, who cares!
		return this._textSlice(1);
	}

	/**
	 * Parse the escape sequence and turn it into some structured thing-a-majig.
	 * https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
	 * @returns A structured ANSIEvent or nothing.
	 */
	private _parseEscapeSequence(): ANSIEvent | null {
		switch (this._internalBuffer[1]) {
			case 91:
				return this._parseCSI();
			default:
				return this._parseAltSequence();
		}
	}

	private _parseAltSequence(): ANSIEvent|null {
		if (this._internalBuffer.length > 1) {
			const ch = String.fromCharCode(this._internalBuffer[1]);
			this._slice(2);
			return {kind: 'alt', key: ch};
		}

		return null;
	}

	/**
	 * Search for the end of the CSI sequence, and slice it out.
	 * @return A structured ANSIEvent or nothing.
	 */
	private _parseCSI(): ANSIEvent | null {
		for (let i = 2; i < this._internalBuffer.length; i++) {
			if (this._internalBuffer[i] >= 0x40 && this._internalBuffer[i] <= 0x7E) {
				const CSI = this._slice(i + 1);
				if (CSI) {
					return {kind: 'CSI', CSI};
				}
			}
		}

		return null;
	}

	private _textSlice(len: number): ANSIEvent | null {
		const text = this._slice(len);
		if (text) {
			return {kind: 'text', text};
		}

		return null;
	}

	private _slice(len: number): string | null {
		if (this._internalBuffer.length >= len) {
			const slice = this._internalBuffer.slice(0, len);
			this._internalBuffer = this._internalBuffer.slice(len);
			return slice.toString();
		}

		return null;
	}
}
