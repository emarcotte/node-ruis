import fs from 'fs';
import util from 'util';
import {Readable, Writable} from 'stream';
import termios from 'termios';
import ioctl from 'ioctl';
import ref from 'ref';
import structType from 'ref-struct';
import GraphemeSplitter from 'grapheme-splitter';
import {Keys, UTF8ANSITransform} from './utf8-ansi-transform';

type ANSIEvent = {kind: 'text'; text: string}
| {kind: 'key'; key: number}
| {kind: 'CSI'; CSI: string}
| {kind: 'alt'; key: string}
| {kind: 'EOF'; EOF: true};

const TIOCGWINSZ = 0x00005413;
const WinSize = structType({
	/* eslint-disable @typescript-eslint/camelcase  */
	ws_row: ref.types.ushort,
	ws_col: ref.types.ushort,
	ws_xpixel: ref.types.ushort,
	ws_ypixel: ref.types.ushort,
	/* eslint-enable @typescript-eslint/camelcase  */
});

/**
 * The callback interface for raw text completion (replace whole line).
 * @param completions Completions to add.
 */
export type CompleterCallback = (...completions: string[]) => void;

/**
 * User-defined completion callback to find what completions should be added.
 * @param line The line we've seen thus far.
 * @param addCompletion Callback function to add completions for the line.
 */
export type Completer = (line: string, addCompletion: CompleterCallback) => void;

export interface NoiseArgs {
	debugLog?: string;
	completer?: Completer;
	inputFD?: number;
	input?: Readable;
	output?: Writable;
	lineHandler: (line: string|null) => void;
}

export class Noise {
	/** Prompt text. */
	private _prompt = 'Prompt> ';

	/** Number of columns in terminal. TODO: Calculate and update. */
	private _cols = 80;

	/** Current cursor position. */
	private _pos = 0;

	/** Previous refresh cursor position. */
	private _oldpos = 0;

	/** Max number of rows seen for this line. */
	private _maxrows = 0;

	private _stdinFileDescriptor: number;

	private _originalTermiosFlags = {};

	private _hasReset = false;

	private _lineBuffer = '';

	private _needsClear: boolean;

	private _inputStream: Readable;

	private _outputStream: Writable;

	private _debugConsole?: number;

	private _completer?: Completer;

	private _ansiTransform: UTF8ANSITransform;

	private _lineHandler: (line: string|null) => void;

	public constructor(args: NoiseArgs) {
		this._inputStream = args.input || process.stdin;
		this._outputStream = args.output || process.stdout;
		this._completer = args.completer;
		this._stdinFileDescriptor = args.inputFD || 0;
		this._lineHandler = args.lineHandler;
		this._ansiTransform = new UTF8ANSITransform();
		this._inputStream.pipe(this._ansiTransform);

		this._needsClear = this._stdinFileDescriptor === 0 && this._isTTY();
		if (this._needsClear) {
			this._originalTermiosFlags = this._getTermiosFlags();
			process.on('exit', () => {
				this._reset();
			});
		}

		if (args.debugLog) {
			this._debugConsole = fs.openSync(args.debugLog, 'w');
		}
	}

	public clearTerminal(): void {
		this._write('\u001B[H\u001B[2J');
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	public debug(format: any, ...args: any): void {
		if (this._debugConsole) {
			fs.writeSync(
				this._debugConsole,
				util.formatWithOptions({colors: true}, format as string, ...args) + '\n'
			);
		}
	}

	/**
	 * Start handling lines...
	 */
	public async run(): Promise<void> {
		this._setRawMode();
		this.getColumns();
		await this._write(this._prompt);
		let exit = false;

		while (!exit) {
			// The loop here needs to process things sequentially.
			/* eslint-disable no-await-in-loop */
			const event = await this._nextEvent();
			exit = await this._handleANSIEvent(event);
			/* eslint-enable no-await-in-loop */
		}
	}

	/* Try to get the number of columns in the current terminal, using ioctl()
	 * first, then anis control sequences if that fails. */
	public async getColumns(): Promise<number> {
		if (this._isTTY()) {
			const ws = new WinSize();
			const ioctlResult = ioctl(1, TIOCGWINSZ, ws.ref());

			if (ioctlResult !== -1 && ws.ws_col !== 0) {
				this._cols = ws.ws_col;
				return this._cols;
			}
		}

		return this.getColumnsANSI();
	}

	public async getColumnsANSI(): Promise<number> {
		if (this._isTTY()) {
			/* Get the initial position so we can restore it later. */
			const start = await this._getCursorPosition();
			if (start.col !== -1) {
				/* Go to right margin and get position. */
				await this._write('\u001B[999C');
				const rightPosition = await this._getCursorPosition();
				if (rightPosition.col !== -1) {
					/* Restore position. */
					if (rightPosition.col > start.col) {
						this._write(`\u001B[${rightPosition.col - start.col}D`);
						this._cols = rightPosition.col;
						return this._cols;
					}
				}
			}
		}

		return this._cols;
	}

	/**
	 * Determines if we have anything left to transform.
	 */
	public hasOutstandingInput(): boolean {
		return this._ansiTransform.hasOutstandingInput();
	}

	/**
	 * Get the current contents of the prompt.
	 */
	public getCurrentBuffer(): string {
		return this._lineBuffer.toString()
	}

	private _getTermiosFlags(): {} {
		return termios.getattr(this._stdinFileDescriptor);
	}

	private async _nextEvent(): Promise<ANSIEvent> {
		if (this._ansiTransform.readable && this._ansiTransform.readableLength > 0) {
			return this._ansiTransform.read() as ANSIEvent;
		}

		return new Promise<ANSIEvent>(resolve => {
			this._ansiTransform.once('readable', () => {
				resolve(this._ansiTransform.read() as ANSIEvent);
			});
		});
	}

	private async _handleANSIEvent(event: ANSIEvent): Promise<boolean> {
		switch (event.kind) {
			case 'EOF':
				await this._lineHandler(null);
				this._lineBuffer = '';
				return true;
			case 'key':
				await this._handleKey(event.key);
				return false;
			case 'CSI':
				await this._handleCSI(event.CSI);
				return false;
			case 'text':
				await this._addText(event.text);
				return false;
			default:
				this.debug('MISSING EVENT', [event]);
				return false;
		}
	}

	private async _addText(text: string): Promise<void> {
		const priorSize = this._length();
		this._lineBuffer = this._lineBuffer.slice(0, this._pos) + text +
			this._lineBuffer.slice(this._pos);

		if (priorSize !== this._length()) {
			this._pos++;
		}

		if (this._pos === this._length()) {
			return this._write(text);
		}

		return this._refreshLine();
	}

	private async _handleCSI(csi: string): Promise<void> {
		switch (csi) {
			case '\u001B[D':
				return this._moveLeft();
			case '\u001B[C':
				return this._moveRight();
			case '\u001B[1~':
				this._pos = 0;
				return this._refreshLine();
			case '\u001B[3~':
				return this._delete();
			case '\u001B[4~':
				this._pos = this._length();
				return this._refreshLine();
			default:
				this.debug('Missing CSI handler for', [csi]);
		}
	}

	private _isTTY(): boolean {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		return (this._inputStream as any).isTTY;
	}

	private _setRawMode(): void {
		if (!this._isTTY()) {
			return;
		}

		termios.setattr(this._stdinFileDescriptor, {
			/*
			 * local modes - echoing off, canonical off, no extended functions,
			 * no signal chars (^Z,^C)
			 */
			lflag: {
				ECHO: false,
				ICANON: false,
				IEXTEN: false,
				ISIG: false,
			},
			/*
			 * input modes: no break, no CR to NL, no parity check, no strip
			 * char, no start/stop output control.
			 */
			iflag: {
				BRKINT: false,
				ICRNL: false,
				INPCK: false,
				ISTRIP: false,
				IXON: false,
			},
			/* output modes - disable post processing */
			oflag: {
				OPOST: false,
			},
			/* control modes - set 8 bit chars */
			cflag: {
				CS8: true,
			},
		});
	}

	private async _handleKey(key: number): Promise<void> {
		switch (key) {
			case Keys.CTRL_C:
			case Keys.CTRL_D:
				this._reset();
				return;
			case Keys.ENTER:
			case Keys.NEWLINE:
				await this._finishBuffer();
				return;
			case Keys.TAB:
				this._fireCompletion();
				return;
			case Keys.BACKSPACE:
				this._backspace();
				return;
			default:
				this.debug(`Missing special event ${key}`);
		}
	}

	private async _moveLeft(): Promise<void> {
		if (this._pos > 0) {
			this._pos--;
			return this._refreshLine();
		}
	}

	private async _moveRight(): Promise<void> {
		if (this._pos < this._length()) {
			this._pos++;
			return this._refreshLine();
		}
	}

	private async _delete(): Promise<void> {
		if (this._pos < this._length()) {
			this._lineBuffer = this._lineBuffer.substr(0, this._pos) +
				this._lineBuffer.substr(this._pos + 1);
			return this._refreshLine();
		}
	}

	private async _backspace(): Promise<void> {
		if (this._pos > 0) {
			const splitter = new GraphemeSplitter();
			const graphemes = splitter.splitGraphemes(this._lineBuffer);
			this._lineBuffer = graphemes.slice(0, this._pos - 1).join('') +
				graphemes.slice(this._pos).join('');
			this._pos--;
			return this._refreshLine();
		}
	}

	private async _refreshLine(): Promise<void> {
		const plen = this._prompt.length;
		let rows = Math.floor((plen + this._lineBuffer.length + this._cols - 1) / this._cols);
		const rpos = Math.floor((plen + this._oldpos + this._cols) / this._cols);
		const oldRows = this._maxrows;
		let refreshText = '';

		/* Update maxrows if needed. */
		if (rows > this._maxrows) {
			this._maxrows = rows;
		}

		/* First step: clear all the lines used before. To do so start by
		 * going to the last row. */
		if (oldRows - rpos > 0) {
			refreshText += `\u001B[${oldRows - rpos}B`;
		}

		/* Now for every row clear it, go up. */
		for (let j = 0; j < oldRows - 1; j++) {
			refreshText += '\r\u001B[0K\u001B[1A';
		}

		/*
		 * Clean the top line, then write the prompt and the current buffer
		 * content
		 */
		refreshText += `\r\u001B[0K${this._prompt}${this._lineBuffer}`;

		/* Show hits if any. */
		// refreshShowHints(&ab,l,plen);

		/* If we are at the very end of the screen with our prompt, we need to
		 * emit a newline and move the prompt to the first column. */
		if (
			this._pos &&
			this._pos === this._lineBuffer.length &&
			(this._pos + plen) % this._cols === 0
		) {
			refreshText += '\n\r';
			rows++;
			if (rows > this._maxrows) {
				this._maxrows = rows;
			}
		}

		/* Move cursor to right position. */
		const rpos2 = Math.floor((plen + this._pos + this._cols) / this._cols);

		/* Go up till we reach the expected positon. */
		if (rows - rpos2 > 0) {
			refreshText += `\u001B[${rows - rpos2}A`;
		}

		/* Set column. */
		const col = (plen + this._pos) % this._cols;
		if (col) {
			refreshText += `\r\u001B[${col}C`;
		}
		else {
			refreshText += '\r';
		}

		this._oldpos = this._pos;
		return this._write(refreshText);
	}

	private async _finishBuffer(): Promise<void> {
		this._lineBuffer += '\r';
		this.debug('Buf done', this._lineBuffer);
		this._write('\r\n');
		this._clearRawMode();
		await this._lineHandler(this._lineBuffer);
		this._setRawMode();
		this._lineBuffer = '';
		this._pos = 0;
		return this._write(this._prompt);
	}

	private _length(): number {
		const splitter = new GraphemeSplitter();
		return splitter.countGraphemes(this._lineBuffer);
	}

	private async _fireCompletion(): Promise<void> {
		if (this._completer) {
			let options: string[] = [];
			this._completer(this._lineBuffer, (...o: string[]) => {
				options = options.concat(o);
			});
			if (options.length === 1) {
				this._lineBuffer = options[0];
				this._pos = this._length();
				this._refreshLine();
			}
			else {
				const cols = await this.getColumns();
				await this._write(`\r\n${this._getOptionText(cols, options)}${this._prompt}${this._lineBuffer}`);
			}
		}
	}

	private _getOptionText(cols: number, options: string[]): string {
		let maxOption = 0;
		let optionText = '';
		for (const option of options) {
			maxOption = Math.max(option.length + 1, maxOption);
		}

		const cellsPerRow = Math.floor(cols / maxOption);
		const cellSize = Math.floor(cols / cellsPerRow);
		let cell = 0;

		for (const option of options) {
			optionText += this._rpad(option, cellSize);
			cell++;
			if (cell === cellsPerRow) {
				optionText += '\r\n';
				cell = 0;
			}
		}

		if (cell !== 0) {
			optionText += '\r\n';
		}

		return optionText;
	}

	private _rpad(str: string, len: number): string {
		return str + ' '.repeat(Math.max(0, len - str.length));
	}

	private async _write(out: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			this._outputStream.write(out, err => {
				if (err) {
					reject(err);
				}
				else {
					resolve();
				}
			});
		});
	}

	private async _getCursorPosition(): Promise<{row: number; col: number}> {
		const size = {row: -1, col: -1};
		this._setRawMode();
		this._write('\u001B[6n');
		// Read the response: "ESC [ rows ; cols R"
		const e = await this._nextEvent();
		if (e.kind === 'CSI') {
			// eslint-disable-next-line no-control-regex
			const match = e.CSI.match(/\u001B\[(\d+);(\d+)R/);
			if (match) {
				size.row = Number(match[1]);
				size.col = Number(match[2]);
			}
		}

		this._clearRawMode();
		return size;
	}

	private _clearRawMode(): void {
		if (this._needsClear) {
			termios.setattr(
				this._stdinFileDescriptor,
				this._originalTermiosFlags
			);
		}
	}

	private _reset(): void {
		if (this._hasReset) {
			return;
		}

		this._clearRawMode();
		this._hasReset = true;
		this._inputStream.unpipe(this._ansiTransform);
		this._ansiTransform.end();
		this._inputStream.destroy();
	}
}
