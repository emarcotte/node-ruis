'use strict';

module.exports = {
	"check-coverage": true,
	all: true,
	extends: "@istanbuljs/nyc-config-typescript",
	extension: [ ".ts" ],
	include: [ "src/**.ts" ],
};
