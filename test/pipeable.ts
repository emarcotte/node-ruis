import {spawn} from 'child_process';
import path from 'path';
import test from 'ava';

test('Can read input from a pipe', t => {
	t.plan(2);
	return new Promise(resolve => {
		const app = [path.join(__dirname, '..', 'bin', 'example.ts')];
		if (process.env.NYC_ROOT_ID) {
			try {
				app.unshift(require.resolve('nyc/bin/wrap.js'));
			}
			catch {
			}
		}

		app.unshift('-r', 'ts-node/register/transpile-only');
		const subprocess = spawn('node', app);
		let buffer = Buffer.from('');
		subprocess.stdout.on('data', (d: Buffer) => {
			buffer = Buffer.concat([buffer, d]);
		});
		subprocess.on('exit', c => {
			t.is(c, 0, 'Clean exit code');
			t.snapshot(buffer.toString(), 'Got piped inputs EOF');
			resolve();
		});
		subprocess.stdin.write('helloz\n');
		// This is a placeholder. Need to improve how well non terminal
		// responses to queries are handled.
		subprocess.stdin.end('size\n');
	});
});
