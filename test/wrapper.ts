import path from 'path';
import {AnsiTerminal} from 'node-ansiterminal';
import AnsiParser from 'node-ansiparser';
import {IPty, spawn} from 'node-pty';
import test, {ExecutionContext} from 'ava';
import {Keys} from '../src/utf8-ansi-transform';

interface TermOps {
	width?: number;
}

class StrongerTerminal extends AnsiTerminal {
	public readonly proc: IPty;

	public constructor(proc: IPty, opt?: TermOps) {
		super(opt && opt.width ? opt.width : 80, 6, 0);
		this.proc = proc;
		this.send = (response: string): void => this.proc.write(response);
	}
}

class TestTerminal {
	public readonly term: StrongerTerminal;

	public readonly parser: AnsiParser;

	public readonly proc: IPty;

	public readonly exitPromise: Promise<number>;

	public constructor(opts?: TermOps) {
		const app = [path.join(__dirname, '..', 'bin', 'example.ts')];

		if (process.env.NYC_ROOT_ID) {
			try {
				app.unshift(require.resolve('nyc/bin/wrap.js'));
			}
			catch {
			}
		}

		app.unshift('-r', 'ts-node/register/transpile-only');

		this.proc = spawn('node', app, {
			name: 'xterm-256color',
			cols: 80,
			rows: 30,
		});

		this.term = new StrongerTerminal(this.proc, opts);
		this.parser = new AnsiParser(this.term);

		this.exitPromise = this._getExitPromise();
		this.proc.on('data', d => this.parser.parse(d));
	}

	public write(payload: string): void {
		this.proc.write(payload);
	}

	public toHTML(): string {
		let data = '';
		for (let i = 0; i < this.term.screen.buffer.length; ++i) {
			data += this.term.screen.buffer[i].toHTML({
				rtrim: false,
				// eslint-disable-next-line @typescript-eslint/camelcase
				empty_cell: ' ',
				classes: false,
			});
			data += '\n';
		}

		return data;
	}

	public async readUntil(re: RegExp): Promise<string> {
		return new Promise<string>(resolve => {
			let line = '';
			const handler = (d: string): void => {
				line += d;
				if (re.test(line)) {
					// TODO: Upgrade node-pty
					// eslint-disable-next-line @typescript-eslint/no-explicit-any
					(this.proc as any).removeListener('data', handler);
					resolve(line);
				}
			};

			this.proc.on('data', handler);
		});
	}

	private _getExitPromise(): Promise<number> {
		return new Promise<number>((resolve, reject) => {
			let exited = false;
			const timeout = setTimeout(() => {
				if (!exited) {
					this.proc.kill();
					reject(new Error('Process did not exit on time'));
				}
			}, 10000);

			this.proc.on('exit', (exitCode: number) => {
				exited = true;
				clearTimeout(timeout);
				resolve(exitCode);
			});
		});
	}
}

interface Input {
	name: string;
	wait: RegExp;
	input: string;
}

async function inputTest(t: ExecutionContext, opts: TermOps, inputs: Input[]): Promise<void> {
	const term = new TestTerminal(opts);
	await term.readUntil(/Prompt> /);

	for (const input of inputs) {
		term.write(input.input);
		// need to serialize await here to make sure we test each point individually
		// eslint-disable-next-line no-await-in-loop
		await term.readUntil(input.wait);
		t.snapshot(term.toHTML(), input.name);
	}

	term.proc.kill();

	t.is(await term.exitPromise, 0);
}

/*
 * These tests use the `inputTest` macro to send some content and then pump the
 * event loop until some output comes through. If that output is never going to
 * come, the tests will timeout, making it somewhat tricky to debug.
 *
 * For now, these tests rely on snapshots to track regressions, but should
 * really be able to use assertions in the future.
 *
 * These tests are set to run serially as it seems they run a little smoother in
 * CI, perhaps due to limited processing power?
 */
test.serial('Backspace support', inputTest, {}, [
	{
		name: 'Backspace deletes some text, new text is appended',
		input: `hellor${String.fromCharCode(Keys.BACKSPACE)}zz\r`,
		wait: /Line was: [\s\S]*Prompt> /,
	},
	{
		name: 'backspace on an empty line does nothing',
		input: String.fromCharCode(Keys.BACKSPACE) + '\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
	{
		name: 'Handle simple graphemes with backspace',
		input: `texth֯${String.fromCharCode(Keys.BACKSPACE)}\r`,
		wait: /Line was: [\s\S]*Prompt> /,
	},
]);

test.serial('Clear support', inputTest, {}, [
	{
		name: 'Clear clears the screen',
		input: 'test\rclear\r',
		wait: /Okie[\s\S]*Prompt/,
	},
]);

test.serial('Home/end motion support', inputTest, {}, [
	{
		name: 'Home key shifts to beginning of line',
		input: 'world\u001B[1~hello \r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
	{
		name: 'End key shifts to the end of the line',
		input: 'world\u001B[1~hello \u001B[4~!!\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
]);

test.serial('Left/right motion support', inputTest, {}, [
	{
		name: 'Left and right keys shift around the buffer',
		input: 'b\u001B[Da\u001B[Cc\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
	{
		name: 'Left and right will not overrun ends of buffer',
		input: 'b\u001B[D\u001B[Da\u001B[C\u001B[Cc\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
]);

test.serial('Delete key support', inputTest, {}, [
	{
		name: 'delete key deletes things',
		input: 'ahello delete\u001B[1~\u001B[3~\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
	{
		name: 'delete will not overrun end of buffer',
		input: 'a\u001B[1~\u001B[3~\u001B[3~no extra delete\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
]);

test.serial('Limited alt-key support', inputTest, {}, [
	{
		name: 'basic alt-letter support',
		input: 'alt\u001BB pressed\r',
		wait: /Line was: [\s\S]*Prompt> /,
	},
]);

test.serial('Basic Ctrl-c support', async t => {
	const term = new TestTerminal();
	await term.readUntil(/Prompt/);
	term.write(String.fromCharCode(Keys.CTRL_C));
	await term.readUntil(/Line was: null/);
	t.is(await term.exitPromise, 0);
});

test.serial('Generic size callbacks support', inputTest, {}, [
	{
		name: 'Size command',
		wait: /Columns: [\s\S]*Prompt> /,
		input: 'size\r',
	},
]);

test.serial('ANSI fallback size callbacks support', inputTest, {}, [
	{
		name: 'Size command',
		wait: /Columns ansi: [\s\S]*Prompt> /,
		input: 'size 1\r',
	},
]);

test.serial('Async line handler support', inputTest, {}, [
	{
		name: 'Sleep command',
		wait: /Super refreshed[\s\S]*Prompt> /,
		input: 'sleep\r',
	},
]);

test.serial('Tab completion in a smaller window wraps', inputTest, {width: 40}, [
	{
		name: 'Tab from nothing',
		input: '\t',
		wait: /clear/,
	},
]);

