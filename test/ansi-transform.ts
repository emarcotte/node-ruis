import {PassThrough} from 'stream';
import test from 'ava';
import {ANSIEvent, UTF8ANSITransform} from '../src/utf8-ansi-transform';

function makeTestPipe(): {writable: PassThrough; found: ANSIEvent[]} {
	const writable = new PassThrough();
	const transform = new UTF8ANSITransform();
	const found: ANSIEvent[] = [];
	transform.on('data', (d: ANSIEvent) => found.push(d));
	writable.pipe(transform);
	return {writable, found};
}

test('Partial escape sequence parsing', t => {
	const {writable, found} = makeTestPipe();

	writable.write(Buffer.from([0x1B]));
	t.deepEqual(found, [], 'No events generated from partial sequence');
	writable.write(Buffer.from('B'));
	t.deepEqual(found, [{kind: 'alt', key: 'B'}], 'Next byte finishes off previous event');
});

test('Partial CSI sequence parsing', t => {
	const {writable, found} = makeTestPipe();

	writable.write(Buffer.from([0x1B, 91]));
	t.deepEqual(found, [], 'No events generated from partial sequence');
	writable.write(Buffer.from('3'));
	t.deepEqual(found, [], 'Still no events with more of the sequence');
	writable.write(Buffer.from('~'));
	t.deepEqual(found, [{CSI: '\u001B[3~', kind: 'CSI'}], 'Finish the sequence');
	writable.write(Buffer.from('h'));
	t.deepEqual(
		found,
		[{CSI: '\u001B[3~', kind: 'CSI'}, {text: 'h', kind: 'text'}],
		'Sequence moves onto other structures'
	);
});

test('Partial utf8 sequences of various lengths', t => {
	const {writable, found} = makeTestPipe();

	writable.write(Buffer.from([0xF0]));
	t.deepEqual(found, [], 'Need more bytes (1/4)');
	writable.write(Buffer.from([0xAB]));
	t.deepEqual(found, [], 'Need more bytes (2/4)');
	writable.write(Buffer.from([0x9D]));
	t.deepEqual(found, [], 'Need more bytes (3/4)');
	writable.write(Buffer.from([0x91]));
	t.deepEqual(found, [{text: '𫝑', kind: 'text'}], 'Have all bytes (4/4)');
	found.pop();

	writable.write(Buffer.from([0xE6]));
	t.deepEqual(found, [], 'Need more bytes (1/3)');
	writable.write(Buffer.from([0x88]));
	t.deepEqual(found, [], 'Need more bytes (2/3)');
	writable.write(Buffer.from([0x91]));
	t.deepEqual(found, [{text: '我', kind: 'text'}], 'Have all bytes (3/3)');
	found.pop();

	writable.write(Buffer.from([0xD1]));
	t.deepEqual(found, [], 'Need more bytes (1/2)');
	writable.write(Buffer.from([0xB0]));
	t.deepEqual(found, [{text: 'Ѱ', kind: 'text'}], 'Have all bytes (2/2)');
	found.pop();

	writable.write(Buffer.from('h'));
	t.deepEqual(found, [{text: 'h', kind: 'text'}], 'Resumes normal parsing');
});
