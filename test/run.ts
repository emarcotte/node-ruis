import fs from 'fs';
import sinon from 'sinon';
import test from 'ava';
import tmp from 'tmp';
import {CompleterCallback, Noise, NoiseArgs} from '../src';

interface ReadHandle {
	path: string;
	fd: number;
	readable: fs.ReadStream;
}

async function createFileHandle(): Promise<Handle> {
	return new Promise<Handle>((resolve, reject) => {
		tmp.file((err, path, fd) => {
			if (err) {
				reject(err);
			}

			resolve({path, fd});
		});
	});
}

async function createReadable(): Promise<ReadHandle> {
	const {path, fd} = await createFileHandle();
	const readable = fs.createReadStream('', {fd});
	return {path, fd, readable};
}

interface WriteHandle {
	path: string;
	fd: number;
	writable: fs.WriteStream;
}

async function createWritable(): Promise<WriteHandle> {
	const {path, fd} = await createFileHandle();
	const writable = fs.createWriteStream('', {fd});
	return {path, fd, writable};
}

interface Handle { path: string; fd: number }

interface TestNoise {
	n: Noise;
	input: ReadHandle;
	output: WriteHandle;
}

async function makeNoise(options: NoiseArgs): Promise<TestNoise> {
	const input = await createReadable();
	const output = await createWritable();

	const n = new Noise({
		completer: options.completer,
		input: input.readable,
		inputFD: input.fd,
		lineHandler: options.lineHandler || (() => {}),
		output: output.writable,
	});

	return {
		n,
		input,
		output,
	};
}

test('Parses multibyte characters properly', async t => {
	t.plan(4);
	const spy = sinon.spy();

	const {n, input} = await makeNoise({
		lineHandler: spy,
	});

	fs.appendFileSync(input.path, 'Ѱ我𫝑\r');

	await n.run();

	t.deepEqual(spy.getCall(0).args, ['Ѱ我𫝑\r'], 'Got the full string');
	t.deepEqual(spy.getCall(1).args, [null], 'Got EOF null');
	t.assert(spy.callCount == 2, 'Invoked handler twice');
	t.assert(!n.hasOutstandingInput(), 'Buffer should be empty');
});

test('Handles backspace properly', async t => {
	t.plan(5);
	const spy = sinon.spy();

	const {n, input} = await makeNoise({
		lineHandler: spy,
	});

	fs.appendFileSync(input.path, 'hellor\u007Fzz\r\u007F\r\u0003');

	await n.run();

	t.deepEqual(spy.getCall(0).args, ['hellozz\r'], 'Backspace at end of line');
	t.deepEqual(spy.getCall(1).args, ['\r'], 'Backspace on empty line');
	t.deepEqual(spy.getCall(2).args, [null], 'EOF');
	t.assert(spy.callCount == 3, '3 callbacks');
	t.assert(!n.hasOutstandingInput(), 'Buffer should be empty');
});

test('Can expand simple commands with TAB key', async t => {
	t.plan(8);
	const completer = sinon.stub()
		.onCall(0).callsFake(
			(line: string, addCompletion: CompleterCallback) => {
				t.assert(line == 'h', 'First invocation is "h"');
				addCompletion('ha there', 'hb there', 'hello', 'hib there', 'hthere', 'hz there');
			}
		)
		.onCall(1).callsFake(
			(line: string, addCompletion: CompleterCallback) => {
				t.assert(line == 'x', 'Second call is "X"');
				addCompletion('xxx');
			}
		);

	let noise: TestNoise;

	const lineHandler = sinon.stub()
		.onCall(0)
		.callsFake(() => {
			t.assert(noise.n.getCurrentBuffer() == 'xxx', 'Line callback invoked with tabbed result');
		});

	noise = await makeNoise({
		completer,
		lineHandler,
	});

	fs.appendFileSync(noise.input.path, Buffer.from('h\t\u007Fx\t'));

	await noise.n.run();

	t.assert(completer.callCount == 2, 'Two callbacks');
	t.deepEqual(lineHandler.getCall(0).args, [null]);
	t.assert(lineHandler.callCount == 1, "Line handler called one time at the end.");

	const outputString = fs.readFileSync(noise.output.path).toString();
	t.regex(outputString, /Prompt> h\r\n/);
	t.regex(outputString, /Prompt> xxx/);
});

test('Ignores unhandled key codes', async t => {
	t.plan(4);
	const spy = sinon.spy();

	const {n, input} = await makeNoise({
		lineHandler: spy,
	});

	fs.appendFileSync(input.path, 'hello\u0015\r');

	await n.run();

	t.deepEqual(spy.getCall(0).args, ['hello\r'], 'Handled the line');
	t.deepEqual(spy.getCall(1).args, [null], 'EOF');
	t.assert(spy.callCount == 2, '2 callbacks');
	t.assert(!n.hasOutstandingInput(), 'Buffer should be empty');
});

test('Ignores unknown CSI codes', async t => {
	t.plan(4);
	const spy = sinon.spy();

	const {n, input} = await makeNoise({
		lineHandler: spy,
	});

	fs.appendFileSync(input.path, 'hello\u001B[Z\r');

	await n.run();

	t.deepEqual(spy.getCall(0).args, ['hello\r'], 'Handled the line');
	t.deepEqual(spy.getCall(1).args, [null], 'EOF');
	t.assert(spy.callCount == 2, '2 callbacks');
	t.assert(!n.hasOutstandingInput(), 'Buffer should be empty');
});

test('Write failures propagate back to caller', async t => {
	t.plan(1);

	const {n, input, output} = await makeNoise({
		lineHandler: () => {},
	});

	output.writable.end();
	output.writable.write = sinon.stub()
		.onCall(0)
		.callsFake(
			(_, cb: ((error: Error) => void)) => {
				cb(new Error('Boom'));
			}
		);

	fs.appendFileSync(input.path, Buffer.from('hi\n'));
	await t.throwsAsync(() => n.run(), /Boom/, 'Throws exception');
});
