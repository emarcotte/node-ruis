module.exports = {
	env: {
		node: true
	},

	extends: [
		'plugin:@typescript-eslint/recommended',
	],

	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
	},

	rules: {
		"@typescript-eslint/explicit-function-return-type": [
			"warning",
			{
				allowTypedFunctionExpressions: true,
				allowExpressions: true,
			},
		],

		"@typescript-eslint/indent": ["error", "tab"]
	},
};
