import {CompleterCallback, Noise} from '../src';

function completion(line: string, addCompletion: CompleterCallback): void {
	const options = [
		'hbbbb there',
		'hello there',
		'hello',
		'hib there',
		'hthere',
		'hz there',
		'clear',
	];
	const matching = options.filter(option => {
		return option.startsWith(line);
	});
	addCompletion(...matching);
}

const noise = new Noise({
	completer: completion,
	debugLog: `${__dirname}/../debug.log`,
	lineHandler: async l => {
		if (l === 'clear\r') {
			noise.clearTerminal();
			console.log('Okie Dokie');
		}
		else if (l === 'size 1\r') {
			console.log('Columns ansi:', await noise.getColumnsANSI());
		}
		else if (l === 'size\r') {
			console.log('Columns:', await noise.getColumns());
		}
		else if (l === 'sleep\r') {
			return new Promise(resolve => {
				console.log('Sleeping');
				setTimeout(() => {
					console.log('Super refreshed');
					resolve();
				}, 1000);
			});
		}
		else {
			console.log('Line was:', l);
		}
		return new Promise((resolve) => resolve());
	},
});

(async () => noise.run())();

/*
feed.setCompetionCallback(completion);
feed.loadHistory("history.txt");
feed.setHintsCallback(hints);
feed.setPrompt("hi> ");
feed.setInputCallbck(input);

function input(line) {
	printf("echo: '%s'\n", line);
	feed.addHistory(line);
	feed.saveHistory("history.txt");
}

function hints(line, setHint) {
	if (line == "hello") {
		setHint(" World", 35, true);
	}
}
 */
