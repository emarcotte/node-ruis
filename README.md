Something like a version of https://github.com/antirez/linenoise/ in node

# Goals:

- [x] Basic prompt motions (left, right, backspace, delete)
- [ ] Terminal resize support
- [ ] Token-based readline alternative, including whitespace/quoting support
- [ ] Full-featured tab completion system
  - [ ] Token-based options including whitespace/quoting
  - [ ] Free-form completer allowing complete text replacement
- [ ] Token-based tab completion system
- [ ] History support
- [ ] Unicode support
- [ ] Application testing utilities
- [ ] Minimal dependencies
- [ ] Useful documentation
- [x] Strongly typed implementation
- [x] Learn some stuff about 1970s technology and how to implement it in 2010s
  technology

# Usage:

The default usage is intended to be similar to the nodejs built in `readline`
module. A sample program can be found in [`bin/examle.ts`](bin/example.ts).

NOTE: The library does not yet support tokens, so we're stuck with lines of text
for now.

The basic gist is:

```
function completer(line: string, addCompletion: CompleterCallback): void {
	const options = [
		'optiona',
		'optionb',
	];
	const matching = options.filter(option => {
		return option.startsWith(line);
	});
	addCompletion(...matching);
}

function handler(line: string) {
	console.log('Line was:', line);
}

const noise = new Noise({
	completer: completer,
	lineHandler: handler,
});

noise.run();
```
