export as namespace ansiparser;

export interface TerminalObject {
	/* eslint-disable @typescript-eslint/camelcase */
	/* eslint-disable @typescript-eslint/no-explicit-any */
	inst_p(s: any): void;
	inst_o(s: any): void;
	inst_x(flag: any): void;
	inst_c(collected: string, params: any[], flag: string): void;
	inst_e(collected: any[], flag: any): void;
	inst_H(collected: any[], params: any, flag: any): void;
	inst_P(dcs: any): void;
	inst_U(): void;
	/* eslint-enable @typescript-eslint/camelcase */
	/* eslint-enable @typescript-eslint/no-explicit-any */
}

export default class AnsiParser {
	public constructor(term: TerminalObject);

	public parse(str: string): void;
}
