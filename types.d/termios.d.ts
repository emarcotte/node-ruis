export as namespace termios;

export interface TermAttributes {
	iflag?: {
		IGNBRK?: boolean;
		BRKINT?: boolean;
		IGNPAR?: boolean;
		PARMRK?: boolean;
		INPCK?: boolean;
		ISTRIP?: boolean;
		INLCR?: boolean;
		IGNCR?: boolean;
		ICRNL?: boolean;
		IXON?: boolean;
		IXOFF?: boolean;
		IXANY?: boolean;
		IMAXBEL?: boolean;
	};
	oflag?: {
		OPOST?: boolean;
		ONLCR?: boolean;
		OCRNL?: boolean;
		ONOCR?: boolean;
		ONLRET?: boolean;
	};
	cflag?: {
		CSIZE?: boolean;
		CS5?: boolean;
		CS6?: boolean;
		CS7?: boolean;
		CS8?: boolean;
		CSTOPB?: boolean;
		CREAD?: boolean;
		PARENB?: boolean;
		PARODD?: boolean;
		HUPCL?: boolean;
		CLOCAL?: boolean;
	};
	lflag?: {
		ECHOKE?: boolean;
		ECHOE?: boolean;
		ECHOK?: boolean;
		ECHO?: boolean;
		ECHONL?: boolean;
		ECHOPRT?: boolean;
		ECHOCTL?: boolean;
		ISIG?: boolean;
		ICANON?: boolean;
		IEXTEN?: boolean;
		EXTPROC?: boolean;
		TOSTOP?: boolean;
		FLUSHO?: boolean;
		PENDIN?: boolean;
		NOFLSH?: boolean;
		XCASE?: boolean;
	};
	cbaud?: number;
}

export function setattr(fd: number, attr: TermAttributes): void;

export function getattr(fd: number): TermAttributes;
