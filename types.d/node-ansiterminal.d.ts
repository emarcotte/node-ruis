import { TerminalObject } from 'node-ansiparser';

export as namespace ansiterminal;

interface HTMLArgs {
	rtrim: boolean;
	empty_cell: string;
	classes: boolean;
}

export declare class TRow {
	public toHTML(args: HTMLArgs): string;
}

export declare class TScreen {
	public readonly buffer: TRow[];
}

export declare class AnsiTerminal implements TerminalObject {
	public constructor(cols: number, rows: number, scrollLen: number);

	public send(response: string): void;

	public readonly screen: TScreen;

	/* eslint-disable @typescript-eslint/camelcase, @typescript-eslint/no-explicit-any */
	public inst_p(s: any): void;
	public inst_o(s: any): void;
	public inst_x(flag: any): void;
	public inst_c(collected: string, params: any[], flag: string): void;
	public inst_e(collected: any[], flag: any): void;
	public inst_H(collected: any[], params: any, flag: any): void;
	public inst_P(dcs: any): void;
	public inst_U(): void;
	public toString(opt?: any): string;
	/* eslint-enable @typescript-eslint/no-explicit-any, @typescript-eslint/camelcase */
}
