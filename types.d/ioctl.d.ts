export default function ioctl(fd: number, request: number, output?: Buffer): number;

export as namespace ioctl;
